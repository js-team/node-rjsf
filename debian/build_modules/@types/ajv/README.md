# Installation
> `npm install --save @types/ajv`

# Summary
This package contains type definitions for ajv (https://github.com/epoberezkin/ajv).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/types-2.0/ajv

Additional Details
 * Last updated: Tue, 29 Nov 2016 23:19:07 GMT
 * File structure: ProperModule
 * Library Dependencies: none
 * Module Dependencies: none
 * Global values: Ajv

# Credits
These definitions were written by York Yao <https://github.com/plantain-00/>.
